# VT Webapp Template

A responsive VT Web template targeted at Web applications with thoughtful consideration for common views
encountered in modern Web applications. The design approach is both minimalistic and unopinionated. There is very
little customization of Bootstrap styles beyond fonts and defining background colors. While the general page layout
(nav, header, main, footer) ought to be suitable for most Web applications with little configuration beyond
making navigation links application-specific, content is intended to be handled by combination of individual
Bootstrap styles in whatever way meets the needs of the application.

## Getting Started
A Dockerized nginx server is provided to start a Web server where you can interact with the template. The only
prerequisite is a bash shell and a Docker daemon:

    bin/run.sh

The live template is accessible at http://localhost:8080/.

## Screenshots
![](screenshots/desktop.png)
![](screenshots/mobile.png)
