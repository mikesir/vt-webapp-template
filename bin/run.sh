#!/bin/bash

IMAGE_NAME=vt-webapp-template
IMGID=$(docker ps | grep "$IMAGE_NAME" | awk '{print $1}')
if [ ! -z "$IMGID" ]; then
  docker kill $IMGID
  docker rm $IMAGE_NAME
fi

docker run --name $IMAGE_NAME \
  -p 8080:80 \
  -v $(pwd)/nginx/conf.d:/etc/nginx/conf.d:ro \
  -v $(pwd)/template:/usr/share/nginx/html:ro \
  -d nginx
